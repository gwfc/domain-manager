import { Component }    from '@angular/core';
import { Router }       from '@angular/router';

import { AuthService }  from '../shared/auth/auth.service';

/**
 * This class represents the lazy loaded RegisterComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.css']
})
export class RegisterComponent {
    model: any = {};

    errorMessage: string;

    constructor(
        private authService: AuthService,
        private router: Router,
    ) {}

    register() {
        this.authService.register(this.model.username, this.model.password, this.model.email)
            .subscribe(
                result => this.registrationSuccessful(),
                error => this.showError(error)
            )
    }

    private registrationSuccessful() {
        this.router.navigate(['/dashboard']);
    }

    private showError(errors: any) {
        this.clearErrors();

        this.model.non_field_errors = errors.non_field_errors;
        this.model.email_errors = errors.email
        this.model.username_errors = errors.username
        this.model.password_errors = errors.password
    }

    private clearErrors() {
        this.model.non_field_errors = undefined;
        this.model.username_errors = undefined;
        this.model.password_errors = undefined;
        this.model.email_errors = undefined;
    }
}
