import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomainDetailsComponent } from './domain-details.component';
import { DomainDetailsRoutingModule } from './domain-details-routing.module';

@NgModule({
  imports: [CommonModule, DomainDetailsRoutingModule],
  declarations: [DomainDetailsComponent],
  exports: [DomainDetailsComponent]
})
export class DomainDetailsModule { }
