import { Component, OnInit }              from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { DomainDetails }    from '../shared/models/domain-details';

import { DomainsService }   from '../shared/domains/domains.service';

/**
 * This class represents the lazy loaded DomainDetailsComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-domain-details',
  templateUrl: 'domain-details.component.html',
  styleUrls: ['domain-details.component.css']
})
export class DomainDetailsComponent implements OnInit {
    error: string;
    domain: string;
    domainDetails: DomainDetails;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private domainsService: DomainsService,
    ) {}

    ngOnInit(): void {
		this.activatedRoute.params.subscribe((params: Params) => {
            this.domain = params['domain'];
            this.domainsService.getDetails(this.domain).subscribe(
                result => this.domainDetails = result,
                error => this.error = error
            );
        });
    }

    clearErrors() {
        this.error = null;
    }
}
