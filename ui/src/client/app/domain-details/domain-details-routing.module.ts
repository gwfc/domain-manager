import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DomainDetailsComponent } from './domain-details.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'domain/:domain', component: DomainDetailsComponent }
    ])
  ],
  exports: [RouterModule]
})
export class DomainDetailsRoutingModule { }
