import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { DomainListItem }    from '../shared/models/domain-list-item';
import { DomainsService }    from '../shared/domains/domains.service';
import { AuthService }       from '../shared/auth/auth.service';

/**
 * This class represents the lazy loaded DashboardComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    expiringNowDomains: DomainListItem[];
    expiringMonthDomains: DomainListItem[];

    constructor(
        private domainsService: DomainsService,
        private router: Router,
        private auth: AuthService
    ) {
        if(!this.auth.isLoggedIn()) {
            router.navigate(['/login']);
        }
    }

    ngOnInit(): void {
        this.domainsService.getExpiringNow().subscribe(
            result => this.expiringNowDomains = result
        );
        this.domainsService.getExpiringMonth().subscribe(
            result => this.expiringMonthDomains = result
        );
    }

    toDomain(item: DomainListItem) {
        this.router.navigate(['/domain', item.name]);
    }
}
