import { Component }    from '@angular/core';
import { Router }       from '@angular/router';

import { AuthService }  from '../shared/auth/auth.service';

/**
 * This class represents the lazy loaded LoginComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent {
    model: any = {};

    constructor(
        private authService: AuthService,
        private router: Router,
    ) {}

    login() {
        this.authService.login(this.model.username, this.model.password)
            .subscribe(
                result => this.loginSuccessful(),
                error => this.showError(error)
            )
    }

    private loginSuccessful() {
        this.router.navigate(['/dashboard']);
    }

    private showError(errors: any) {
        this.clearErrors();

        this.model.non_field_errors = errors.non_field_errors;
        this.model.username_errors = errors.username;
        this.model.password_errors = errors.password;
    }

    private clearErrors() {
        this.model.non_field_errors = undefined;
        this.model.username_errors = undefined;
        this.model.password_errors = undefined;
    }
}
