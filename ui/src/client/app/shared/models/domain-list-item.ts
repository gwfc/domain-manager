export class DomainListItem {
    constructor(
		public name: string,
		public type: string,
	) { }
}
