export class DomainDetails {
    constructor(
		public name: string,
        public created_date: string,
        public updated_date: string,
        public expiration_date: string,
	) { }
}
