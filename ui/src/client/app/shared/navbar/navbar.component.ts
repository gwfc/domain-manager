import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
})
export class NavbarComponent {
    authService: any;
    constructor(auth: AuthService) {
        this.authService = auth; //dirty hack :-(
    }
}
