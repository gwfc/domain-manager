import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { Injectable }     				   from '@angular/core';
import { Observable }     				   from 'rxjs/Observable';

import { AuthService }    from '../auth/auth.service';
import { DomainListItem } from '../models/domain-list-item';
import { DomainDetails }  from '../models/domain-details';

/**
 * This class provides the Domains service with CRUD methods
 */
@Injectable()
export class DomainsService {

  private headers: Headers;

  private urlScheme = window.location.protocol;
  private urlHost = window.location.host;
  private baseUrl = `${this.urlScheme}//${this.urlHost}/api/v1`;
  private domainListUrl = `${this.baseUrl}/domains`;
  private domainDetailsUrl = `${this.baseUrl}/domain`;

  /**
   * Creates a new DomainsService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(
      private http: Http,
      private auth: AuthService
  ) {
      this.headers = this.auth.getAuthHeader();
  }

  /**
   * Fetch all user's folders and domains
   * @return {DomainListItem[]} The Observable for DomainListItems
   */
  listFolder(folder: string): Observable<DomainListItem[]> {
      return this.http.get(`${this.domainListUrl}/${folder}`, {headers: this.auth.getAuthHeader()})
        .map(result => result.json() as DomainListItem[])
        .catch(error => Observable.throw(this.handleError(error)));
  }

  /**
   * Add item to specified folder
   */
  addItem(folder: string, item: DomainListItem) {
      return this.http.put(`${this.domainListUrl}/${folder}`, item, {headers: this.auth.getAuthHeader()})
        .map(result => result)
        .catch(error => Observable.throw(this.handleError(error)));
  }

  /**
   * Get details of specified domain name
   * @return {DomainDetails} The Observable for DomainListItems
   */
  getDetails(domain: string): Observable<DomainDetails> {
      return this.http.get(`${this.domainDetailsUrl}/${domain}`, {headers: this.auth.getAuthHeader()})
        .map(result => result.json() as DomainDetails)
        .catch(error => Observable.throw('Server not responding'));
  }

  /**
   * Get list of expiring domains
   * @param {params} string - date period of domain expirtaion
   * @return {DomainListItem[]} The Observable for DomainListItems
   */
  getExpiring(expiration: string): Observable<DomainListItem[]> {
	  let params = new URLSearchParams();
	  params.set('expiring', expiration);

      return this.http.get(`${this.domainListUrl}/`, { headers: this.auth.getAuthHeader(), params:params })
        .map(result => result.json() as DomainListItem[])
        .catch(error => Observable.throw('error fetching expiring domains'));
  }

  /**
   * Get list of expiring now domains
   * @return {DomainListItem[]} The Observable for DomainListItems
   */
  getExpiringNow(): Observable<DomainListItem[]> {
	  return this.getExpiring('now');
  }

  /**
   * Get list of domains expiring in a month
   * @return {DomainListItem[]} The Observable for DomainListItems
   */
  getExpiringMonth(): Observable<DomainListItem[]> {
	  return this.getExpiring('month');
  }

  private handleServerError(error: any) {
      console.log(error);
      return {'non_field_errors': ['Server not responding']};
  }

  private handleError(error: any) {
      if (error.status == 400) {
          return error.json();
      }
      else if (error.status >= 500) {
          return this.handleServerError(error);
      }
      else {
          console.error(error);
          return {'non_field_errors': ['Unknown error']};
      }
  }
}

