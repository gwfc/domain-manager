export class RegistrationRequest {
    constructor(
		public username: string,
        public password: string,
        public email: string,
	) { }
}
