import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { Injectable }     				   from '@angular/core';
import { Observable }     				   from 'rxjs/Observable';

import { LoginRequest }                    from './login-request';
import { RegistrationRequest }             from './registration-request';

/**
 * This class provides the authentication service
 */
@Injectable()
export class AuthService {

  private urlScheme = window.location.protocol;
  private urlHost = window.location.host;
  private baseUrl = `${this.urlScheme}//${this.urlHost}/api/v1`;
  private loginUrl = `${this.baseUrl}/auth/login`;
  private registerUrl = `${this.baseUrl}/auth/register`;

  /**
   * Creates a new AuthService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {}

  /**
   * Saves token to local storage and returns saved token.
   * @param {token} string - Auth token
   * @return {string} Saved token
   */
  private saveToken(token: string): string {
    localStorage.setItem('auth-token', token);
    return token;
  }

  /**
   * Gets token from local storage.
   * @return {string} Saved token
   */
  getToken(): string {
    return localStorage.getItem('auth-token');
  }

  /**
   * Return correct Authentication header with token
   * @return {Header} Auth header
   */
  getAuthHeader(): Headers {
      let headers = new Headers();
      headers.append('Authorization', 'Token ' + this.getToken());
      return headers;
  }

  isLoggedIn(): boolean {
      if (this.getToken() === null) {
          return false;
      } else {
          return true;
      }
  }

  /**
   * Login user using specified username and password
   * After token retrieval it is stored in local storage and can be accessed
   * thru helper method of this service.
   * @return {string} User auth token
   */
  login(username: string, pass: string): Observable<string> {
      let request = new LoginRequest(username, pass)
      return this.http.post(this.loginUrl, request)
        .map(result => this.saveToken(result.json().token))
        .catch(error => Observable.throw(this.handleError(error)));
  }

  /**
   * Register user using specified username, password and email
   * After token retrieval it is stored in local storage and can be accessed
   * thru helper method of this service.
   * @return {string} User auth token
   */
  register(username: string, pass: string, email: string): Observable<string> {
      let request = new RegistrationRequest(username, pass, email)
      return this.http.post(this.registerUrl, request)
        .map(result => this.saveToken(result.json().token))
        .catch(error => Observable.throw(this.handleError(error)));
  }

  /**
   * Handle all server-related errors, that are common in both cases (login and
   * registration). E.g. 502 Bad Gateway
   */
  private handleServerError(error: any) {
      console.error(error);
      return {'non_field_errors': ['Server not responding']};
  }

  /**
   * Handle all auth-related errors
   */
  private handleError(error: any) {
      if (error.status == 400) {
          return error.json();
      }
      else if (error.status > 500) {
          return this.handleServerError(error);
      }
      else {
          console.error(error);
          return {'non_field_errors': ['Unknown error']};
      }
  }
}

