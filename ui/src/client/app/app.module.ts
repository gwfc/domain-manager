import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { DashboardModule } from './dashboard/dashboard.module';
import { LoginModule } from './login/login.module';
import { RegisterModule } from './register/register.module';
import { DomainsListModule } from './domains-list/domains-list.module';
import { DomainDetailsModule } from './domain-details/domain-details.module';
import { SharedModule } from './shared/shared.module';


@NgModule({
  imports: [
      BrowserModule,
      HttpModule,
      AppRoutingModule,
      DashboardModule,
      DomainsListModule,
      LoginModule,
      RegisterModule,
      DomainDetailsModule,
      SharedModule.forRoot()
  ],
  declarations: [AppComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  }],
  bootstrap: [AppComponent]

})
export class AppModule { }
