import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DomainsListComponent } from './domains-list.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
          path: 'domains',
          pathMatch: 'prefix',
          children: [
              {
                  path: '**',
                  component: DomainsListComponent,
              }
          ],
      }
    ])
  ],
  exports: [RouterModule]
})
export class DomainsListRoutingModule { }
