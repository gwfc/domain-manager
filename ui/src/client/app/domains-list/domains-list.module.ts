import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomainsListComponent } from './domains-list.component';
import { DomainsListRoutingModule } from './domains-list-routing.module';

@NgModule({
  imports: [CommonModule, DomainsListRoutingModule],
  declarations: [DomainsListComponent],
  exports: [DomainsListComponent]
})
export class DomainsListModule { }
