import { Component, OnInit }              from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { DomainListItem }    from '../shared/models/domain-list-item';

import { DomainsService }    from '../shared/domains/domains.service';
import { AuthService }    from '../shared/auth/auth.service';

/**
 * This class represents the lazy loaded DomainsListComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-domains-list',
  templateUrl: 'domains-list.component.html',
  styleUrls: ['domains-list.component.css']
})
export class DomainsListComponent implements OnInit {
    items: DomainListItem[];
    folder: string;

    errors: any = {};

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private domainsService: DomainsService,
        private auth: AuthService
    ) {
        if(!this.auth.isLoggedIn()) {
            router.navigate(['/login']);
        }
    }

    ngOnInit(): void {
		this.activatedRoute.url.subscribe((segments: Params) => {
				this.folder = segments.join('/');
                this.fetchDomains(this.folder);
        });
    }

    private fetchDomains(folder: string) {
        this.domainsService.listFolder(folder).subscribe(
            items => this.items = items,
            error => this.showError(error.non_field_errors)
        );
    }

    addDomain(name: string) {
        let item = new DomainListItem(name, 'domain');
        this.addItem(item, error => this.showError(error.name));
    }

    addFolder(name: string) {
        let item = new DomainListItem(name, 'folder');
        this.addItem(item, error => this.showError(error.name));
    }

    addItem(item: DomainListItem, errorHandler: (e: any) => any) {
        this.domainsService.addItem(this.folder, item).subscribe(
            item => this.fetchDomains(this.folder),
            error => errorHandler(error)
        );
    }

    toFolder(item: DomainListItem) {
        this.router.navigate([item.name], {relativeTo: this.activatedRoute} );
    }

    toDomainDetails(item: DomainListItem) {
        this.router.navigate(['/domain', item.name]);
    }

    private clearErrors() {
        this.errors.non_field_errors = null
    }

    private showError(error: any) {
        this.clearErrors();
        this.errors.non_field_errors = error;
    }
    get debug() {
        return this.items;
    }
}
