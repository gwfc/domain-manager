import json
import pytest

from django.contrib.auth.models import User


@pytest.mark.django_db
class TestRegistrationView(object):
    """Test user registration via API"""
    base_route = '/api/v1/auth/register'

    def test_route(self, client):
        """Check that route is accessible"""
        response = client.get(self.base_route)

        assert response.status_code != 404

    @pytest.mark.parametrize("data", [
        {'username': 'test', 'email': 'test@test.com', 'password': 'test'},
        {'username': 'test', 'password': 'test'}
    ])
    def test_user_can_register(self, client, data):
        """Check that we return token after successful user registration"""
        data = json.dumps(data)
        response = client.post(self.base_route, data=data,
                               content_type='application/json')

        user = User.objects.get(username='test')

        assert response.status_code == 201
        assert client.login(username='test', password='test')
        assert response.json() == {'token': user.auth_token.key}

    def test_user_already_exists(self, client):
        """Check that we return nice message when user is already registered"""
        data = json.dumps({'username': 'test', 'email': 'test@test.com',
                           'password': 'test'})
        response = client.post(self.base_route, data=data,
                               content_type='application/json')

        assert response.status_code == 201

        response = client.post(self.base_route, data=data,
                               content_type='application/json')

        expected_json = {
            'username':
                ['A user with that username already exists.'],
        }

        assert response.status_code == 400
        assert response.json() == expected_json


@pytest.mark.django_db
class TestLoginView(object):
    base_route = '/api/v1/auth/login'

    def test_route(self, client):
        """Check that route is accessible"""
        response = client.get(self.base_route)

        assert response.status_code != 404

    def test_registered_user_can_login(self, client):
        """Check that registered user can login"""
        user = User.objects.create_user(username='test', password='test',
                                        email='test@test.com')

        data = json.dumps({'username': 'test', 'password': 'test'})
        response = client.post(self.base_route, data=data,
                               content_type='application/json')

        assert response.status_code == 200
        assert response.json() == {'token': user.auth_token.key}

    def test_not_registered_user_login_fail(self, client):
        data = json.dumps({'username': 'test', 'password': 'test'})
        response = client.post(self.base_route, data=data,
                               content_type='application/json')

        expected_json = {
            'non_field_errors':
                [u'Unable to log in with provided credentials.']
        }
        assert response.status_code == 400
        assert response.json() == expected_json
