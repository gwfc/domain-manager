from django.conf.urls import url
from auth.views import RegisterView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    url(r'^register$', RegisterView.as_view()),
    url(r'^login$', obtain_auth_token),
]
