# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.response import Response
from rest_framework.views import APIView
from auth.serializers import UserSerializer


class RegisterView(APIView):
    """User registration view"""

    def post(self, request):
        data = request.data
        us = UserSerializer(data=data)
        us.is_valid(raise_exception=True)
        user = us.save()

        result = {'token': user.auth_token.key}
        return Response(result, status=201)
