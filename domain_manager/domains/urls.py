from django.conf.urls import url
from domains.views import FolderView, DomainDetailsView

urlpatterns = [
    url(r'^domains/(?P<folder_name>[A-Za-z0-9\/\_\-\.]*)', FolderView.as_view()),
    url(r'^domain/(?P<name>[A-Za-z0-9\.\_\-]+)',
        DomainDetailsView.as_view()),
]
