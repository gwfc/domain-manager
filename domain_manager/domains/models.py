# -*- coding: utf-8 -*-
"""Data model allows to create filesystem-like structure of domains.

Implemented data model allow us to create nested directory structure of
unlimited depth due to use of recursive ``Folder`` model.

.. note::

    Currently we limit depth of folders nesting to 1 for simplicity,
    but no data migration needed to increase it in future.
"""
from __future__ import unicode_literals

import datetime

from django.contrib.auth.models import User as DjangoUser
from django.db import models, IntegrityError

from domains.exceptions import FolderDepthLimitError, FolderAlreadyExistsError
from domains.exceptions import FolderNotFoundError, DomainNotFoundError
from domains.exceptions import DomainAlreadyExistsError


class UserProfile(models.Model):
    """Application-specific user data.

    Extends ``django.contrib.auth.models.User``
    """

    user = models.OneToOneField(DjangoUser)

    @property
    def username(self):
        """Username"""
        return self.user.username

    @property
    def root_folder(self):
        """Root folder"""
        try:
            return self.folder_set.get(name="/")
        except Folder.DoesNotExist:
            # that's very strange edge case, but if somehow user root folder
            # doesn't exist we must shadow Folder.DoesNotExist with our
            # own application exception.
            raise FolderNotFoundError()

    def list_domains(self):
        """Get QuerySet of all user's domains."""
        return Domain.objects.filter(folder__user=self).distinct()

    def list_domains_expire_now(self):
        """Get QuerySet of Domains expiring now"""
        base_qs = self.list_domains()

        return base_qs.filter(expiration_date__lte=datetime.date.today())

    def list_domains_expire_one_month(self):
        """Get QuerySet of Domains expiring in one month"""
        base_qs = self.list_domains()

        today = datetime.date.today()
        date_range = (today, today + datetime.timedelta(30))

        return base_qs.filter(expiration_date__range=date_range)

    def get_folder(self, folder_name):
        """Get folder with specified name.
        This simple method relies on one-tier folder structure.
        """
        if folder_name == '/':
            return self.root_folder

        if '/' in folder_name:
            # for simplicity we assume that shash is a sign of nested folder,
            # and we don't allow to work with folders with nesting level > 1
            raise FolderNotFoundError()

        try:
            return self.folder_set.get(name=folder_name)
        except Folder.DoesNotExist:
            # we shadow Folder.DoesNotExist for exception handling unification
            # of upper layer code
            raise FolderNotFoundError()

    def __str__(self):
        return self.username


class Folder(models.Model):
    """Tree-like model of domain folders.

    Support nesting of folders for implementing filesystem-like approach
    of storing domains.

    Folders are user-specific, so every user have own root folder.
    """

    # this field is used to distinguish folders and domains in UI
    type = 'folder'

    user = models.ForeignKey(UserProfile)
    """Owner of folder"""

    parent = models.ForeignKey('self', null=True, blank=True)
    """Upper level folder. ``None`` if folder is root."""

    name = models.CharField(max_length=64)
    """Folder name. Max length is 64 symbols"""

    domains = models.ManyToManyField("Domain", blank=True)
    """List of domains in folder"""

    def create_subfolder(self, name):
        """Create subfolder in current folder.

        :param name: folder name.
        :return: created Folder instance.

                 If folder nesting limit is reached ``FolderDepthLimitError``
                 will be raised.

                 If folder with specified name already exists
                 ``FolderAlreadyExistsError`` will be raised.
        """
        if self.parent is not None:
            # if there is parent folder we hit the depth limit
            raise FolderDepthLimitError()

        try:
            return Folder.objects.create(user=self.user, parent=self, name=name)
        except IntegrityError:
            raise FolderAlreadyExistsError()

    def delete_subfolder(self, folder):
        """Delete subfolder from current folder.

        :param folder: subfolder name
        :type  folder: Folder or str
        """
        try:
            folder = self.subfolders.get(name=folder)
        except Folder.DoesNotExist:
            raise FolderNotFoundError()

        self.folder_set.remove(folder)

    def add_domain(self, domain):
        """Add domain to current folder.
        If domain already exists in folder `DomainAlreadyExistsError` will be
        raised.

        :param domain: domain
        :type domain:  Domain or str
        :return:       created domain instance
        :rtype:        Domain
        """

        if isinstance(domain, (str, unicode)):
            domain = Domain.objects.get_or_create(name=domain)[0]

        try:
            self.domains.get(name=domain.name)
        except Domain.DoesNotExist:
            self.domains.add(domain)
        else:
            raise DomainAlreadyExistsError()

        return domain

    def delete_domain(self, domain_name):
        """Deletes domain from current folder.

        :param domain_name: domain
        :type  domain_name: Domain or str
        """
        try:
            domain = Domain.objects.get(name=domain_name)
        except Domain.DoesNotExist:
            raise DomainNotFoundError()

        self.domains.remove(domain)

    @property
    def subfolders(self):
        """List of subfolders in folder"""
        return self.folder_set.all()

    class Meta:
        unique_together = ('user', 'parent', 'name')

    def __str__(self):
        return self.name


class Domain(models.Model):
    """Represents domain whois data.

    Domains are unique to avoid duplication.
    """

    # this field is used to distinguish folders and domains in UI
    type = 'domain'

    name = models.CharField(max_length=256, unique=True)
    """Domain name. Max length is 256."""

    created_date = models.DateField(null=True)
    """Date the domain name was registered."""

    updated_date = models.DateField(null=True)
    """Date the domain name record was last modified."""

    expiration_date = models.DateField(null=True)
    """Date the domain name registration expires."""

    def __str__(self):
        return self.name
