from rest_framework import serializers


class ModelException(Exception):
    pass


class FolderDepthLimitError(ModelException):
    """The maximum folder nesting limit is reached"""


class FolderAlreadyExistsError(ModelException):
    """Unable to create subfolder. Folder with the same name already exists"""


class FolderNotFoundError(ModelException):
    """Folder doesn't exist.
    This exceptions must be used to shadow Folder.DoesNotExists for exception
    handling unification.
    """


class DomainNotFoundError(ModelException):
    """Domain doesn't exist.
    This exceptions must be used to shadow Domain.DoesNotExists for exception
    handling unification.
    """


class DomainAlreadyExistsError(ModelException):
    """Unable to add domain to folder. The same domain is already in folder"""


class ValidationError(serializers.ValidationError):
    """Raises 400 Bad Request with correct structure"""

    def __init__(self, detail, field=None, **kwargs):
        if field:
            detail = {field: [detail]}
        super(ValidationError, self).__init__(detail, **kwargs)
