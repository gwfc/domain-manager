import re

from rest_framework import serializers
from domains.models import Folder, Domain


def format_validator(regex, value, error_message):
    """Simple helper for regex validation"""
    if not re.match(regex, value):
        raise serializers.ValidationError(error_message)


class FolderSerializer(serializers.ModelSerializer):
    """`domains.Folder` serializer"""
    name_regex = re.compile('^[A-Za-z0-9\_\-\.]+$')

    class Meta:
        model = Folder
        fields = ('name', 'type')

    def validate_name(self, value):
        """Custom folder name validator"""
        format_validator(
            self.name_regex,
            value,
            ("Folder name can contain only letters, digits, underscodes, "
             "dashes, and dots.")
        )

        return value

    def create(self, validated_data):
        """Simple instance creator.
        It doesn't catch any exceptions to let upper-level code decide how to
        process them.
        """
        parent_folder = validated_data['folder']
        name = validated_data['name']

        return parent_folder.create_subfolder(name)


class DomainSerializer(serializers.ModelSerializer):
    """`domains.Domain` serializer"""
    name_regex = re.compile('^[a-z0-9\.\-\_]+\.[a-z]{2,}$')

    class Meta:
        model = Domain
        fields = ('name', 'type')

        extra_kwargs = {
            'name': {
                'validators': [],
            },
        }

    def validate_name(self, value):
        """Custom domain name validator"""
        format_validator(
            self.name_regex,
            value,
            "Domain name is incorrect"
        )

        return value

    def create(self, validated_data):
        """Simple instance creator.
        It doesn't catch any exceptions to let upper-level code decide how to
        process them.
        """
        parent_folder = validated_data['folder']
        name = validated_data['name']

        return parent_folder.add_domain(name)


class FolderItemSerializer(serializers.Serializer):
    """Used for creation of new folder items (both subfolders and domains)"""
    name = serializers.CharField()
    type = serializers.ChoiceField(['folder', 'domain'])


class DomainDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Domain
        fields = ('name', 'created_date', 'updated_date', 'expiration_date')
