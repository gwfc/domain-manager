from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from domains.models import UserProfile, Folder, Domain
from domains import tasks


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """Create UserProfile for newly created user"""
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=UserProfile)
def create_root_folder(sender, instance, created, **kwargs):
    """Create default root folder for newly created user (via UserProfile)"""
    if created:
        Folder.objects.create(user=instance, name="/")


@receiver(post_save, sender=User)
def create_user_token(sender, instance, created, **kwargs):
    """Create token for new user"""
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=Domain)
def fetch_domain_data(sender, instance, created, **kwargs):
    """Fetch domain info for newly created domains"""
    if created:
        tasks.fetch_domain_data(instance)
