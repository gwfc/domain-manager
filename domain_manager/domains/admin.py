# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import UserProfile, Folder, Domain
# Register your models here.


@admin.register(UserProfile)
class UserAdmin(admin.ModelAdmin):
    pass


@admin.register(Folder)
class FolderAdmin(admin.ModelAdmin):
    pass


@admin.register(Domain)
class DomainAdmin(admin.ModelAdmin):
    pass
