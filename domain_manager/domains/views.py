# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AnonymousUser

from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound

from domains.serializers import FolderSerializer, DomainSerializer
from domains.serializers import FolderItemSerializer, DomainDetailsSerializer
from domains.exceptions import FolderNotFoundError, FolderDepthLimitError
from domains.exceptions import FolderAlreadyExistsError, DomainNotFoundError
from domains.exceptions import DomainAlreadyExistsError, ValidationError
from domains.models import Folder, Domain


class FolderView(APIView):
    """List, create or delete contents of current folder"""
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def _get_user(self, request):
        return request.user.userprofile

    def _get_folder(self, user, folder_name):
        """Get folder with specified name."""
        if folder_name == "":
            folder_name = "/"

        try:
            return user.get_folder(folder_name)
        except FolderNotFoundError:
            raise NotFound()

    def _create_folder(self, folder, item):
        """Create subfolder in specified folder"""
        fs = FolderSerializer(data=item)
        fs.is_valid(raise_exception=True)

        try:
            fs.save(folder=folder)
        except FolderDepthLimitError:
            raise ValidationError('Folder depth limit is exceeded',
                                  field='name')
        except FolderAlreadyExistsError:
            raise ValidationError('Subfolder already exists in this folder',
                                  field='name')

    def _create_domain(self, folder, item):
        """Create domain in specified parent folder.

        All other logic of domain data population must be implemented in
        form of signals to decouple view logic from complex data retrieval
        """
        ds = DomainSerializer(data=item)
        ds.is_valid(raise_exception=True)

        try:
            ds.save(folder=folder)
        except DomainAlreadyExistsError:
            raise ValidationError('Domain already exists in this folder',
                                  field='name')

    def _create_item(self, user, folder_name, item):
        """Create item in specified folder."""
        item_type = item['type']

        folder = self._get_folder(user, folder_name)

        if item_type == 'folder':
            self._create_folder(folder, item)

        elif item_type == 'domain':
            self._create_domain(folder, item)

    def _delete_folder(self, folder, name):
        """Delete subfolder from specified folder"""
        try:
            folder.delete_subfolder(name)
        except FolderNotFoundError:
            raise NotFound()

    def _delete_domain(self, folder, name):
        """Delete domain from specified folder"""
        try:
            folder.delete_domain(name)
        except DomainNotFoundError:
            raise NotFound()

    def _delete_item(self, user, folder_name, item):
        """Delete item from specified folder."""
        item_type = item['type']
        item_name = item['name']

        folder = self._get_folder(user, folder_name)

        if item_type == 'folder':
            self._delete_folder(folder, item_name)

        elif item_type == 'domain':
            self._delete_domain(folder, item_name)

    def _is_filtered(self, request):
        """Checks if request request contains filtering params and check
        value of that param"""
        param = request.query_params.get('expiring')

        return (param and param in ('now', 'month'))

    def _get_filter(self, request):
        """Returns filtering value"""
        return request.query_params['expiring']

    def _get_filtered_subfolders(self, request, folder):
        """Get subfolders of specified folder with filtering applied.

        If request is filtered it should return empty QuerySet.
        """
        if self._is_filtered(request):
            return Folder.objects.none()  # TODO: remove abstraction violation
        else:
            return folder.subfolders

    def _get_filtered_domains(self, request, folder):
        """Get domains of specified folder with filtering applied.

        ..note:
            Currently it ignores specified folder and uses root user folder
            for filtering of domains
        """
        user = folder.user
        queryset = None

        if self._is_filtered(request):
            param = self._get_filter(request)
            if param == 'now':
                queryset = user.list_domains_expire_now()
            elif param == 'month':
                queryset = user.list_domains_expire_one_month()
        else:
            queryset = folder.domains

        return queryset

    def get(self, request, folder_name):
        """Get folder contents"""
        user = self._get_user(request)
        folder = self._get_folder(user, folder_name)

        subfolders = self._get_filtered_subfolders(request, folder)
        domains = self._get_filtered_domains(request, folder)

        sf = FolderSerializer(subfolders, many=True).data
        df = DomainSerializer(domains, many=True).data

        return Response(sf + df)

    def put(self, request, folder_name):
        """Put new object to folder.

        Object can be subfolder or domain.

        Currently only one item per request is supported, but in future it can
        be changed with low impact on API."""
        item = FolderItemSerializer(data=request.data)
        item.is_valid(raise_exception=True)

        user = self._get_user(request)
        self._create_item(user, folder_name, item.data)

        return Response(status=201)

    def delete(self, request, folder_name):
        """Delete object from folder.

        Object can be subfolder or domain"""
        item = FolderItemSerializer(data=request.data)
        item.is_valid(raise_exception=True)

        user = self._get_user(request)
        self._delete_item(user, folder_name, item.data)

        return Response(status=204)


class DomainDetailsView(RetrieveAPIView):
    """Generic RetrieveAPIView for domain data retrieving"""
    queryset = Domain.objects.all()
    serializer_class = DomainDetailsSerializer
    lookup_field = 'name'
