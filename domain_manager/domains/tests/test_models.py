import itertools
import pytest

from django.contrib.auth.models import User
from domains.models import UserProfile, Folder, Domain
from domains.exceptions import FolderDepthLimitError, FolderAlreadyExistsError
from domains.exceptions import FolderNotFoundError, DomainNotFoundError
from domains.exceptions import DomainAlreadyExistsError


@pytest.fixture
def domains(domain, time_delta):
    """Returns list of Domain objects"""
    not_expired = [
        domain('example.com'),
        domain('yandex.ru'),
        domain('google.com'),
    ]

    expiring_now = [
        domain('ya.ru', expiration_date=time_delta(0)),
        domain('mail.ru', expiration_date=time_delta(-5)),
    ]

    expiring_in_one_month = [
        domain('rambler.ru', expiration_date=time_delta(25)),
    ]

    return itertools.chain(not_expired, expiring_now, expiring_in_one_month)


@pytest.fixture
def user_domains(test_user):
    """Returns QuerySet of test_user domains"""
    return Domain.objects.filter(folder__user=test_user)


@pytest.mark.django_db
class TestUserModel(object):
    def test_user_profile_created_on_new_user_registration(self):
        """Check if new domains.UserProfile instance is created on
        django.contrib.auth.models.User creation.
        """
        User.objects.create_user(username="testing", email="t@t.com",
                                 password="testing_pass")

        UserProfile.objects.get(user__username="testing")

    def test_user_token_created_on_new_user_registration(self):
        """Check that new Token instance created on user registration"""
        user = User.objects.create_user(username="testing", email="t@t.com",
                                        password="testing_pass")

        assert user.auth_token

    def test_root_folder_created_on_new_user_profile(self, test_user):
        """Check if new root Folder object instance is created automatically
        on UserProfile creation
        """
        folder = Folder.objects.get(name="/")

        assert folder.user == test_user

    def test_get_root_folder(self, test_user):
        folder = test_user.root_folder

        assert folder.user == test_user
        assert folder.name == "/"

    def test_get_root_folder_not_found(self, test_user):
        """Check very strange edge where user's root folder doesn't exist."""
        test_user.root_folder.delete()

        with pytest.raises(FolderNotFoundError):
            test_user.root_folder


    def test_list_user_domains(self, test_user, test_user_root, domain):
        """Check retrieving of all domains of current user.
        Domains should be returned as QuerySet object"""

        domain1 = domain('example.com')
        domain2 = domain('test.com')
        domain3 = domain('google.com')

        subfolder1 = test_user_root.create_subfolder("folder1")
        subfolder2 = test_user_root.create_subfolder("folder2")

        test_user_root.add_domain(domain1)
        subfolder1.add_domain(domain2)
        subfolder2.add_domain(domain3)
        subfolder2.add_domain(domain1)

        expected_domains = list(
                Domain.objects.filter(folder__user=test_user).distinct()
        )

        assert expected_domains == list(test_user.list_domains())

    def test_list_user_domains_expiring_now(self, test_user, test_user_root,
                                            domains, time_delta, user_domains):
        """Check retrieving of all expiring now domains of current user"""
        for domain in domains:
            test_user_root.add_domain(domain)

        expiring_now = user_domains.filter(expiration_date__lte=time_delta(0))

        assert list(expiring_now) == list(test_user.list_domains_expire_now())

    def test_list_user_domains_expiring_in_one_month(self, test_user,
                                                     test_user_root, domains,
                                                     time_delta, user_domains):
        """Check retrieving of all expiring in a month domains of current user
        """
        for domain in domains:
            test_user_root.add_domain(domain)

        today = time_delta(0)
        date_range = (today, time_delta(30))

        expected_domains = list(
            user_domains.filter(expiration_date__range=date_range)
        )

        expiring_in_one_month = list(test_user.list_domains_expire_one_month())

        assert expected_domains == expiring_in_one_month

    @pytest.mark.parametrize("folder_name", [
        'testing_folder',
    ])
    def test_get_user_subfolder(self, test_user, test_user_root, folder_name):
        """Check that it's possible to retrieve subfolder of current user"""
        subfolder = test_user_root.create_subfolder(folder_name)

        assert test_user.get_folder(folder_name) == subfolder

    def test_get_user_root_folder(self, test_user):
        """Check that get_folder returns root folder on '/' folder"""
        assert test_user.get_folder('/') == test_user.root_folder

    @pytest.mark.parametrize("folder_name", [
        'testing_folder',
        '/nested/folders/are/not/supported/',
    ])
    def test_get_user_subfolder_not_found(self, test_user, folder_name):
        """Check that get_folder returns FolderNotFoundError"""
        with pytest.raises(FolderNotFoundError):
            test_user.get_folder(folder_name)


@pytest.mark.django_db
class TestFolderModel(object):
    def test_create_subfolder_successfully(self, test_user, test_user_root):
        subfolder = test_user_root.create_subfolder("test")

        assert subfolder == Folder.objects.get(name="test", user=test_user)

    def test_create_subfolder_failed_depth_limit(self, test_user_root):
        """Check that we can't create subfolder with nesting depth > 1"""
        subfolder = test_user_root.create_subfolder("level1")

        with pytest.raises(FolderDepthLimitError):
            subfolder.create_subfolder("level2")

    def test_duplicate_folders_not_allowed(self, test_user_root):
        """Check that we can't create two subfolders with the same name"""
        test_user_root.create_subfolder("subfolder")

        with pytest.raises(FolderAlreadyExistsError):
            test_user_root.create_subfolder("subfolder")

    def test_add_domain_creates_new_domain(self, test_user_root):
        """Check that we can create new Domain object if pass string 'name'
        parameter to Folder.add_domain"""
        test_user_root.add_domain('example.com')

        assert test_user_root.domains.all()[0].name == 'example.com'

    def test_add_domain_adds_existing_domain(self, test_user_root, domain):
        """Check that we can add existing Domain object if pass string 'name'
        parameter to Folder.add_domain"""
        domain('example.com')

        test_user_root.add_domain('example.com')

        assert test_user_root.domains.all()[0].name == 'example.com'

    def test_duplicate_domains_not_allowed(self, test_user_root):
        """Check that we can't add the same domain in one folder twice"""
        test_user_root.add_domain('example.com')

        with pytest.raises(DomainAlreadyExistsError):
            test_user_root.add_domain('example.com')

    def test_delete_domain(self, test_user_root, domain):
        """Check that we can remove domain from current folder.
        Ensure that we can remove domain using Domain instance or name str"""
        domain1 = domain('example.com')
        domain2 = 'plain-text-domain.com'
        test_user_root.add_domain(domain1)
        test_user_root.add_domain(domain2)

        test_user_root.delete_domain(domain1)
        test_user_root.delete_domain(domain2)

        assert test_user_root.domains.all().count() == 0

    def test_delete_domain_not_found(self, test_user_root):
        """Check that we raise custom DomainNotFoundError exception if
        domain is not found"""

        with pytest.raises(DomainNotFoundError):
            test_user_root.delete_domain('not-exists.com')

    def test_delete_subfolder(self, test_user_root):
        """Check that we can remove folder from current folder.
        Ensure that we can remove folder using Folder instance or it's name"""
        folder1 = test_user_root.create_subfolder('folder1')
        test_user_root.create_subfolder('folder2')

        test_user_root.delete_subfolder(folder1)
        test_user_root.delete_subfolder('folder2')

        assert test_user_root.subfolders.count() == 0

    def test_delete_subfolder_not_found(self, test_user_root):
        """Check that we raise custom FolderNotFoundError exception if
        folder is not found"""

        with pytest.raises(FolderNotFoundError):
            test_user_root.delete_subfolder('not_exist')


@pytest.mark.django_db
class TestDatabaseCleanup(object):
    """Garbage collection tests"""

    @pytest.mark.xfail
    def test_all_user_folders_are_removed_after_root(self):
        """Check that we remove all subfolders of user"""
        assert 0, 'write the test'

    @pytest.mark.xfail
    def test_all_folder_to_domain_relations_are_removed(self):
        """Check that we remove folder-to-domain relations after folder removal
        """
        assert 0, 'write the test'
