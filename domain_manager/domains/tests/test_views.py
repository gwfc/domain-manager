import json
import pytest

from domains.models import Domain


class DomainListCommon(object):
    base_route = '/api/v1/domains/'


class TestFolderViewGet(DomainListCommon):

    @pytest.mark.parametrize("route", [
        '/api/v1/domains',
        '/api/v1/domains/',
        '/api/v1/domains/root',
        '/api/v1/domains/deep/folder/',
    ])
    def test_routes(self, client, route):
        """Check that handler url is accessable"""
        response = client.get(route)

        assert response.status_code != 404

    @pytest.mark.parametrize("method", [
        'get',
        'put',
        'delete',
    ])
    def test_anonymous_user_forbidden(self, client, method):
        """Check that application return 403 Forbidden to not authorized user
        """
        http_method = getattr(client, method)
        response = http_method(self.base_route)

        assert response.status_code == 403

    def test_registered_user_root_folder_list(self, test_user_root, domain,
                                              authed_client):
        """Check that authenticated user view contents of his root folder"""
        test_user_root.create_subfolder("testing")
        test_user_root.add_domain(domain("example.com"))

        response = authed_client.get(self.base_route)

        expected_json = [
            {'name': 'testing', 'type': 'folder'},
            {'name': 'example.com', 'type': 'domain'},
        ]

        assert response.json() == expected_json

    def test_registered_user_subfolder_list(self, test_user_root, domain,
                                            authed_client):
        """Check that authed user can view contents of subfolders"""
        subfolder = test_user_root.create_subfolder("testing")
        subfolder.add_domain(domain("example.com"))

        response = authed_client.get(self.base_route + 'testing')

        expected_json = [
            {'name': 'example.com', 'type': 'domain'},
        ]

        assert response.json() == expected_json

    def test_registered_user_subfolder_not_found(self, authed_client):
        """Check that we return nice FolderNotFound error message with 404 code
        """
        response = authed_client.get(self.base_route + 'not_exist')

        assert response.status_code == 404
        assert response.json() == {'detail': 'Not found.'}


class TestFolderViewGetFiltered(DomainListCommon):
    base_route = '/api/v1/domains/'

    def test_domains_expiring_now(self, test_user_root, exp_now_domain,
                                  authed_client, exp_mon_domain):
        """Check that user can get list of domains expiring now"""
        subfolder = test_user_root.create_subfolder('subfolder')

        test_user_root.add_domain(exp_now_domain('example.com'))
        subfolder.add_domain(exp_now_domain('test.com'))

        test_user_root.add_domain(exp_mon_domain('google.com'))
        test_user_root.add_domain(exp_mon_domain('yandex.ru'))

        response = authed_client.get(self.base_route, {'expiring': 'now'})

        expected_json = [
            {'name': 'example.com', 'type': 'domain'},
            {'name': 'test.com', 'type': 'domain'},
        ]

        assert response.status_code == 200
        assert response.json() == expected_json

    def test_domains_expiring_one_month(self, test_user_root, exp_now_domain,
                                        authed_client, exp_mon_domain, domain):
        """Chech that user can get list of domains expiring in one month"""
        subfolder = test_user_root.create_subfolder('subfolder')

        # Add expiring domains
        test_user_root.add_domain(exp_mon_domain('google.com'))
        subfolder.add_domain(exp_now_domain('test.com'))
        subfolder.add_domain(exp_mon_domain('yandex.ru'))

        # Add not expiring domains
        test_user_root.add_domain(domain('rambler.ru'))
        test_user_root.add_domain(domain('www.ru'))

        response = authed_client.get(self.base_route, {'expiring': 'month'})

        expected_json = [
            {'name': 'google.com', 'type': 'domain'},
            {'name': 'test.com', 'type': 'domain'},
            {'name': 'yandex.ru', 'type': 'domain'},
        ]

        assert response.status_code == 200
        assert response.json() == expected_json


class TestFolderViewPut(DomainListCommon):
    def test_registered_user_can_create_subfolder(self, authed_client,
                                                  test_user):
        """Check that authed user can create subfolder"""

        data = json.dumps({'name': 'subfolder', 'type': 'folder'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        assert response.status_code == 201

        test_user.get_folder('subfolder')

    def test_registered_user_create_subfolder_invalid_req(self, authed_client):
        """Check that we return ValidationError on invalid input"""
        data = json.dumps({'name': 'subfolder'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        assert response.status_code == 400
        assert response.json() == {'type': ['This field is required.']}

    def test_registered_user_create_item_invalid_type(self, authed_client):
        """Check that we return InvalidRequest on invalid item type"""
        data = json.dumps({'name': 'subfolder', 'type': 'invalid_type'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        expected_json = {'type': ['"invalid_type" is not a valid choice.']}

        assert response.status_code == 400
        assert response.json() == expected_json

    def test_registered_user_create_subfolder_not_found(self, authed_client):
        """Check that we return 404 Not Found on item creation if folder
        is missing"""
        data = json.dumps({'name': 'subfolder', 'type': 'folder'})
        response = authed_client.put(self.base_route + 'some_dir', data=data,
                                     content_type='application/json')

        assert response.status_code == 404
        assert response.json() == {'detail': 'Not found.'}

    def test_registered_user_create_subfolder_nesting_lim(self, authed_client,
                                                          test_user_root):
        """Check that we return 400 on folder nesting limit"""
        test_user_root.create_subfolder("subfolder")

        data = json.dumps({'name': 'second_folder', 'type': 'folder'})
        response = authed_client.put(self.base_route + 'subfolder', data=data,
                                     content_type='application/json')

        expected_json = {
            'name': ['Folder depth limit is exceeded']
        }
        assert response.status_code == 400
        assert response.json() == expected_json

    def test_registered_user_cant_create_dup_folders(self, authed_client,
                                                     test_user_root):
        """Check that we can't add the same folder twice"""
        data = json.dumps({'name': 'subfolder', 'type': 'folder'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        assert response.status_code == 201

        data = json.dumps({'name': 'subfolder', 'type': 'folder'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        expected_json = {
            'name': ['Subfolder already exists in this folder']
        }
        assert response.status_code == 400
        assert response.json() == expected_json

    def test_registered_user_can_create_domain(self, authed_client, test_user):
        """Check that user can add domain to existing folder"""
        data = json.dumps({'name': 'example.com', 'type': 'domain'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        assert response.status_code == 201

        domain = test_user.list_domains()[0]

        assert domain.name == 'example.com'

    def test_registered_user_can_create_dup_domain_into_diff_folder(self,
        authed_client, test_user_root):
        """Check that we can add the same domain into different folders"""
        test_user_root.create_subfolder('test1')

        data = json.dumps({'name': 'example.com', 'type': 'domain'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        assert response.status_code == 201

        data = json.dumps({'name': 'example.com', 'type': 'domain'})
        response = authed_client.put(self.base_route + 'test1', data=data,
                                     content_type='application/json')

        assert response.status_code == 201

    def test_registered_user_cant_create_dup_domains(self, authed_client,
                                                     test_user_root):
        """Check that user can't add duplicate domain to the same folder"""
        data = json.dumps({'name': 'example.com', 'type': 'domain'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        assert response.status_code == 201

        data = json.dumps({'name': 'example.com', 'type': 'domain'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        expected_json = {
            'name': ['Domain already exists in this folder']
        }
        assert response.status_code == 400
        assert response.json() == expected_json


class TestFolderViewDelete(DomainListCommon):
    def test_registered_user_can_delete_folder(self, authed_client,
                                               test_user_root):
        """Check that user can remove existing subfolder"""
        test_user_root.create_subfolder("test")

        data = json.dumps({'name': 'test', 'type': 'folder'})
        response = authed_client.delete(self.base_route, data=data,
                                        content_type='application/json')

        assert response.status_code == 204
        assert test_user_root.subfolders.count() == 0

    def test_registered_user_folder_not_found(self, authed_client):
        """Check that we return nice 404 message if subfolder not found"""
        data = json.dumps({'name': 'test', 'type': 'folder'})
        response = authed_client.delete(self.base_route, data=data,
                                        content_type='application/json')

        assert response.status_code == 404
        assert response.json() == {'detail': 'Not found.'}

    def test_registered_user_can_delete_domain(self, authed_client, test_user,
                                               test_user_root, domain):
        """Check that user can remove existing domain from his folder.
        Domain instance should still be available"""
        test_domain = domain('example.com')
        test_user_root.add_domain(test_domain)

        data = json.dumps({'name': 'example.com', 'type': 'domain'})
        response = authed_client.delete(self.base_route, data=data,
                                        content_type='application/json')

        assert response.status_code == 204
        assert test_user.list_domains().count() == 0
        assert Domain.objects.filter(name='example.com').exists()

    def test_registered_user_domain_not_found(self, authed_client):
        """Check that we return nice 404 message if domain not found"""
        data = json.dumps({'name': 'example.com', 'type': 'domain'})
        response = authed_client.delete(self.base_route, data=data,
                                        content_type='application/json')

        assert response.status_code == 404
        assert response.json() == {'detail': 'Not found.'}


class TestFolderViewDataValidation(object):
    base_route = '/api/v1/domains/'

    @pytest.mark.parametrize("name,is_valid", [
        ('testing_folder',      True),
        ('testing-folder',      True),
        ('folder/with/slashes', False),
        ('strang#_$ymbols',     False),
        ('1234567',             True),
    ])
    def test_create_subfolder_name_validation(self, authed_client, name,
                                              is_valid):
        """Check that we accept valid folder name and reject invalid with
        appropriate error message"""
        data = json.dumps({'name': name, 'type': 'folder'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        if is_valid:
            assert response.status_code == 201
        else:
            expected_json = {
                'name': [
                    "Folder name can contain only letters, digits, "
                    "underscodes, dashes, and dots."
                ]
            }

            assert response.status_code == 400
            assert response.json() == expected_json

    @pytest.mark.parametrize("name", [
        'a' * 65,
        'a' * 128,
    ])
    def test_create_subfolder_name_len_validation(self, authed_client, name):
        """Check that we reject folder names with length > 64"""
        data = json.dumps({'name': name, 'type': 'folder'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        expected_json = {
            'name': [
                'Ensure this field has no more than 64 characters.'
            ]
        }

        assert response.status_code == 400
        assert response.json() == expected_json

    @pytest.mark.parametrize("name,is_valid", [
        ('google.com',  True),
        ('do-main.ru',  True),
        ('bbc.co.uk',   True),
        ('12345.ru',    True),
        ('.ru',         False),
        ('3$.ru',       False),
    ])
    def test_create_domain_name_validation(self, authed_client, name, is_valid):
        """Check that we accept valid domain name and reject invalid with
        appropriate error message"""
        data = json.dumps({'name': name, 'type': 'domain'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        if is_valid:
            assert response.status_code == 201

        else:
            expected_json = {
                'name': [
                    'Domain name is incorrect',
                ]
            }

            assert response.status_code == 400
            assert response.json() == expected_json

    @pytest.mark.parametrize("name", [
        'a' * 257,
        'a' * 1024,
    ])
    def test_create_domain_name_len_validation(self, authed_client, name):
        """Check that we reject domain names with length > 256"""
        data = json.dumps({'name': name, 'type': 'domain'})
        response = authed_client.put(self.base_route, data=data,
                                     content_type='application/json')

        expected_json = {
            'name': [
                'Ensure this field has no more than 256 characters.'
            ]
        }

        assert response.status_code == 400
        assert response.json() == expected_json


@pytest.mark.django_db
class TestDomainDetailsView(object):
    base_route = '/api/v1/domain/'

    @pytest.mark.parametrize("domain_name", [
        'google.com',
        'bbc.co.uk',
        'simple-domain.com',
    ])
    def test_routes(self, client, domain_name, domain):
        """Check that handler url is accessable"""
        domain(domain_name)
        response = client.get(self.base_route + domain_name)

        assert response.status_code != 404

    def test_anonymous_user_have_access(self, client, domain):
        """Check that anonymous user can access domain details"""
        test_domain = domain('example.com')

        response = client.get(self.base_route + test_domain.name)
        result = response.json()

        isodate = lambda attr: getattr(test_domain, attr).isoformat()

        assert response.status_code == 200
        assert result['updated_date'] == isodate('updated_date')
        assert result['created_date'] == isodate('created_date')
        assert result['expiration_date'] == isodate('expiration_date')

    def test_registered_user_have_access(self, authed_client, domain):
        """Check that registered user can access domain details"""
        test_domain = domain('example.com')

        response = authed_client.get(self.base_route + test_domain.name)
        result = response.json()

        isodate = lambda attr: getattr(test_domain, attr).isoformat()

        assert response.status_code == 200
        assert result['updated_date'] == isodate('updated_date')
        assert result['created_date'] == isodate('created_date')
        assert result['expiration_date'] == isodate('expiration_date')

    def test_domain_not_found(self, client):
        """Check that we return nice error message if DomainNotFound"""
        response = client.get(self.base_route + 'google.com')

        assert response.status_code == 404
        assert response.json() == {'detail': 'Not found.'}
