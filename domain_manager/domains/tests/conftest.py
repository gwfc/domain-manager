import datetime
import pytest

from django.contrib import auth
from domains.models import UserProfile, Domain


@pytest.fixture
def test_user_name():
    return 'test'


@pytest.fixture
def test_user_pass():
    return 'testing'


@pytest.fixture
def test_user(db, test_user_name, test_user_pass):
    """Creates django user and returns associated domains.models.UserProfile"""
    user = auth.models.User.objects.create_user(username=test_user_name,
                                                email='test@test.com',
                                                password=test_user_pass)
    return user.userprofile


@pytest.fixture
def test_user_root(test_user):
    """Returns test_user root_folder"""
    return test_user.root_folder


@pytest.fixture
def domain(db):
    """Creates and returns Domain instances with specified `name` and
    `expiration_date`"""
    def func(name, expiration_date=None):
        if not expiration_date:
            expiration_date = datetime.date.today() + datetime.timedelta(365)

        return Domain.objects.create(
            name=name,
            created_date=datetime.date(1970, 1, 1),
            updated_date=datetime.date.today() - datetime.timedelta(365),
            expiration_date=expiration_date
        )

    return func


@pytest.fixture
def time_delta():
    """Returns ``datetime.date`` with specified timedelta in days"""
    def func(days):
        return datetime.date.today() + datetime.timedelta(days)
    return func


@pytest.fixture
def authed_client(db, client, test_user, test_user_name, test_user_pass):
    client.login(username=test_user_name, password=test_user_pass)
    return client


@pytest.fixture
def exp_now_domain(domain, time_delta):
    """Creates and returns Domain instance with today() expiration date"""
    def func(name):
        return domain(name, expiration_date=time_delta(0))
    return func


@pytest.fixture
def exp_mon_domain(domain, time_delta):
    """Creates and returns Domain instance with today() + 25 days expiration"""
    def func(name):
        return domain(name, expiration_date=time_delta(25))
    return func
