from threading import Thread
from functools import wraps
from pythonwhois import get_whois

from domains.models import Domain


def thread(func):
    """Simple threading decorator. Starts function in separate daemon thread.
    Thread must be daemon to allow us to stop application gracefully.

    In real world it's better to use ThreadPoolExecutor to avoid huge number
    of threads.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        thread = Thread(target=func, args=args, kwargs=kwargs)
        thread.daemon = kwargs.get('daemon', True)
        thread.start()
    return wrapper


@thread
def fetch_domain_data(domain, **kwargs):
    """Simple task for domain data fetching. It is synchronous, so it must be
    run in separate thread.

    Threading may be not the best solution, but it is simple enough for
    test assignment.

    For production it may be better to use broker-based solution for task
    execution (e.g. RabbitMQ + custom consumers)
    """
    whois = get_whois(domain.name)

    def get_date(attr):
        date = whois.get(attr)
        if date:
            return date[0].date()

    domain.created_date = get_date('creation_date')
    domain.updated_date = get_date('updated_date')
    domain.expiration_date = get_date('expiration_date')

    domain.save()


def update_domains_data(daemon=False):
    """Simple task for massive domain data update. This task fetches all Domain
    records from db and calls ``fetch_domain_data`` for every domain.

    It's not efficient, but simple enough for small amount of domains in db.

    In real world it's highly inefficient to use threading for whois fetching,
    it's better to use async solutions (such as gevent).
    """
    domains = Domain.objects.all()

    for domain in domains:
        fetch_domain_data(domain, daemon=daemon)
