.. Domain Manager documentation master file, created by
   sphinx-quickstart on Wed Mar 29 16:18:10 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Domain Manager
==========================================

Domain Manager is a simple Django application for monitoring of domain names
of your interest.

Features
--------------

Domain Manager offers you following:
    - Personal accounts for domains monitoring
    - Tree-like domain organization. You can organize your domains into 
      folders for easier management
    - Daily updates of domains information
    - Simple API for automation using external scripts


Contents
--------
..  toctree::
    :maxdepth: 2

    overview
    application
