auth package
===========================

auth.serializers module
--------------------------------------

.. automodule:: auth.serializers
    :members:

auth.views module
--------------------------------

.. automodule:: auth.views
    :members:
