Overview
========

Domain Manager is a simple application for domain names monitoring. It can be
used for domain expiration dates monitoring and other related things.


Components
----------

**API Backend**
    Provides unified RESTful interface for UI and custom automation scripts.

    Django application built with `Django Rest Framework
    <http://django-rest-framework.org>`_.

**User Interface**
    Provides what it has to - web user interface for:
        - registration of new users
        - logging in for existing users
        - creation/modifying/deletion of domain names

    Angular2 application.

**Worker**
    Used for scheduled updates of domain data.

    Simple python script for cron.
