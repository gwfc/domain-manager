import domain_manager
from setuptools import setup, find_packages


setup(
    name="DomainManager",
    version=domain_manager.__version__,
    description="Simple application for domain names monitoring",
    author="Nikolay Sedelnikov",
    author_email="n.sedelnikov@gmail.com",
    packages=find_packages(),
    install_requires=[
        'Django==1.11',
        'Pygments==2.2.0',
        'djangorestframework==3.6.2',
        'coreapi==2.3.0',
        'django-cors-headers==2.0.2',
        'pythonwhois==2.4.3',
    ],
    setup_requires=[
    ],
    tests_require=[
    ],
    include_package_data=True,
    zip_safe=True,
)
