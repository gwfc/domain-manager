FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y python-pip nginx supervisor cron

# env configuration
ENV pythonpkg DomainManager-0.1.tar.gz

# backend installation
COPY dist/${pythonpkg} /tmp/backend/
RUN cd /tmp/backend/ && \
    pip install ${pythonpkg} && \
    cd /usr/local/lib/python2.7/dist-packages/domain_manager && \
    python manage.py migrate

# frontend installation
ADD ui/dist/prod /var/www/domain_manager/

# nginx configuration
COPY configs/nginx.conf /etc/nginx/sites-enabled/default
RUN sed -i '1s/^/daemon off;\n/' /etc/nginx/nginx.conf

# add documentation
COPY docs/build/html /var/www/domain_manager/docs

# supervisor configuration
ADD configs/supervisor /etc/supervisor/conf.d/

# add cron task for domain data update
COPY scripts/update_domain_data.py /etc/cron.daily

COPY scripts/run.sh /root/
WORKDIR /root

EXPOSE 80
CMD ["./run.sh"]
