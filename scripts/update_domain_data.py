#!/usr/bin/python2.7

import os
import sys
import django

sys.path.insert(0, os.path.abspath('/usr/local/lib/python2.7/dist-packages/domain_manager/'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "domain_manager.settings")
django.setup()

from domains.tasks import *

update_domains_data()
