cwd = $(shell pwd)

registry_path = ozapinq/domain-manager
container_tag = $(registry_path):$(DM_VERSION)

container_name = angular2-builder
container_not_found = $(shell docker ps -a | grep $(container_name); echo $$?)

build: check_env build_python build_js
	echo "building $(container_tag)..."
	docker build -t $(container_tag) .

build_python: check_env 
	echo "building python package..."
	python setup.py sdist

build_js: check_env 
ifeq ($(container_not_found), 1)
	echo "container not found, creating one..."
	docker run --name $(container_name) -v $(cwd)/ui:/share/ui \
		-w /share/ui node:latest /bin/bash -c "npm install && ./node_modules/.bin/gulp build.bundle.rxjs && npm run build.prod"

else
	echo "container already created, starting it..."
	docker start $(container_name)
endif

publish: check_env
	echo "pushing image to registry..."
	docker push $(container_tag)

clean: clean_python clean_js

clean_python:
	echo "removing python package..."
	rm -r dist || true

clean_js:
	echo "removing build container..."
	docker container rm -f $(container_name) || true

check_env:
ifndef DM_VERSION
	$(error DM_VERSION is undefined)
endif
